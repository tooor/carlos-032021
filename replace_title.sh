#!/bin/sh

COMMIT_HASH_LATEST=`git rev-parse HEAD`
HEADER_FILE="./src/Header.js"
HEADER_DEFAULT_TITLE="Emoji Search"
COMMIT_HASH_REGEX="\b[0-9a-f]{40}\b"

# Replaces the default title if found
grep -q "$HEADER_DEFAULT_TITLE" $HEADER_FILE
if [ $? -eq 0 ]; then
    echo "$HEADER_FILE replacing default title"
    /bin/sed -i "s/$HEADER_DEFAULT_TITLE/$COMMIT_HASH_LATEST/" $HEADER_FILE
    exit
fi

# Replaces git commit hash if found
grep -q -E "$COMMIT_HASH_REGEX" $HEADER_FILE
if [ $? -eq 0 ]; then
    echo "$HEADER_FILE replacing commit hash"
    /bin/sed -i -E "s/$COMMIT_HASH_REGEX/$COMMIT_HASH_LATEST/" $HEADER_FILE
    exit
fi
