Emoji Search
---

emoji-search is a small application that helps you find the best emojis!


### Local Development
Install dependencies
```
npm install
```

Start development server 
```
npm start
```

## Building and Running with Docker
We can use Docker to generate a container image which encompasses our web server and static HTML files.

First build the container image using the docker build command:
```
docker build -f deployments/Dockerfile -t carlos/emoji-search .
```

Then run the container while binding the applications listening port (5000) to localhost:
```
docker run --rm --name emoji-search -it -p 3000:3000 carlos/emoji-search
```

## Run in Kubernetes - Kubectl (yaml)

Note: You must already have a Kubernetes cluster configured and ready for use! You can verify this using `kubectl get nodes`

The k8s.yaml file includes the resources necessary to deploy the emoji-search application to Kubernetes.

First install the resources to your Kubernetes cluster using kubectl:
```
kubectl apply -f k8s.yaml
```

Then start a port forward
```
kubectl port-forward svc/emoji-search 3000:3000
```

Then navigate to http://localhost:3000/ in your browser to access the application.

To clean up the created resources:
```
kubectl delete -f k8s.yaml
```


## Build and Run in Kubernetes - Tilt

Tilt can be used to run the emoji-search application in a local Kubernetes development enviornment.

The following command will start the application with live reload features enabled.
```
tilt up
```
